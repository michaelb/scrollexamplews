## Lorem ipsum

This is a testing document for the Scroll Editor. And here's an example
endnote, hover to see it endnote{Now you've seen it}.

To edit any element, _click on it_.

```
for x in range(3):
    print 'Some cool features include syntax and math', x

$$
\sqrt{x + y}
\over{a_1 + a_2 + ... + a_i}

The remainder of this document is some work-in-progress introductory text for a
forthcoming Scroll site, that provides a broad introduction to the goals of the
Scroll project:

## Some WIP documentation

Scroll is an open source word processing system, that can prepare, edit, and
print documents, as well as export them to a variety of formats. The project is
pretty ambitious.

### For the writer

- Scroll is meant to be robust for your important documents: it leverages the
  power of `git` to auto-save with an undo-history that can never be lost, and
  facilitate online collaboration, and/or storage of your documents on remote
  servers.

- Scroll uses `markdown` to allow for very precise editing, making it well
  suited for preparing things like books (markdown is the approach to adding
  formatting used by Google Talk and Facebook, where `*this*` becomes *this*).

- Scroll is a semantic editor: This means you tell what the text *should mean*
  as opposed to how it should look. Scroll will take care of how it should
  look.

- As an example, instead of enlarging and centering text for a chapter
  heading in your novel, you will instead create the document as a "novel" and
  then create a new "Chapter heading" element. When printing or exporting, it
  will have proper spacing above it, be automatically numbered, have end-notes
  precede it as they should, and have other customizable formatting options
  applied universally across all your chapters.

### For the designer

- Design once, then re-use: Scroll is an ultra-customizable document formatting
  system. Know a little HTML or CSS? You can effortlessly create your own
  "tags", immediately usable in the editor.

- The Style format -- the templates used when exporting -- is also incredibly
  powerful, and can structure your document in any way you wish.

- Tags, Styles, and Export Profiles are bundled with every document, so if you
  perfect a certain editing style, you can send it to others.

### For the CLI power user

- If you feel more at home using your own editor, scroll is un-opinionated. You
  can safely use the CLI tools with your own editor, while collaborating with
  someone using the GUI editor.

- Scroll files are simply tarred `git` repos containing markdown files, along
  with `cfg` files for the document manifest and other parts. Editing these  by
  hand is easy.

- CLI tools are available for exporting, importing, and so on, meaning you can
  easily automate a document editing flow.


### For the programmer

Scroll has a good test suite, including functional testing for both the CLI and
GUI interface.

Scroll is written in  JavaScript, intended for node.js 5+, across several
modules on the Node Package Manager:

- `libscroll` is the core library used by all interfaces. This is useful you
  want to write an application that manipulates scroll files.

- Not yet done: `scrolldata` contains all the default document templates. This
  will be where the essential / default tags library is expanded upon.

- `scrollcli` contains the CLI interface, and requires both of the above. It
  exposes the `scroll` executable.

- `scrolleditor` contains the Electron.js GUI editor, and requires all of the
  above.

Scroll also closely uses a few other NPM modules, that, while completely
separate, were developed originally for scroll:

- `taglex` -- a library for generating custom markdown parsers

- `lovelock` -- a library for driving end-to-end tests in Electron.js

- `schemaconf` -- a library for parsing and serializing the CFG format used by
  scroll

## Robore poenas cetera

### Non habuit novissimus

Lorem ipsum oculos tamen ea fata heu, via est aequor praecontrectatque
aequore! Succedere respicit eripuit inridet parte evanescere, ingeminat rogat
per: pervenientia magis Panthoides nostros.

Exsaturanda quem, rupibus, feras dea latuit arvo mediis pennis. A dempta
memorant manesque, et mentis et tempore tenui quadriiugi inscius recessit
cunctatus coniurata in. Possim finitur frustra propositos et reddit.

### Capacem fontis

Mihi propiorque timor, temptatum, mane Glaucus videndo illuc requiret cum: Dauno
terruit sic gradus illis! Dedi quem inquit; nec sacra hic.

Quo neque sua, mendacem victa decimum nec instat, habetur sortes, infelix nisi.
Amicus dat crescitque pacis frondescere utque temperat quaecumque iam et, in, et
tramite verba hominemque siderea Cephalus in.

## Oscula viridis

Harenae mirabile obit at faces et siquid titulus spiritus animus, ante omnem
fundamina olim cum haec, legi. Huius illis noctem malae glacialis Phoebo parte
et movere interea Iuli: stetit, illa officioque magis? Canisve solet aliquid
caelo de cincta solidoque ad laevi hoc suorum hic, per turba, tu et! Rapto lato
et mihi abdidit tantum fata erit; rigescere finxit et abit orbatura. Ne Delphos
inmanem.

Ut virgo aditus color te ergo germanam huc nisi, ille videtur utque. Cursus ter
loquentes, Alcyone patriaeque admisso alimenta silentum laesi sceleratus dives
cervicibus procorum semine veneno flectit mihi. Ulmi tumidam, iam fera subsedit
quaesitamque solus vetustas magni sit campis fontem memorabat solum ire.
Spoliare est viribus: instabat auxilium iuveni auster.

### Ne in Caras a stabat te iura

Dolor mentas neve ille vellem spatiosa claroque es annos: Achivos consistere
tumulo? Ego summa has corpus. Luco magnus retinens et quid motos cerva cavus
sacerdos lapidoso viris, utque infitianda cuique procos ad sunt scitatur.

In aethera alarum Aeacus cedemus omni, in omnia Faunine, huius. Deae pecus
perfida committat laniare bello; et quot sit.

Ripae nutrit magna. Illum aqua spatium in stipite hinc cervice pateat; apertas
nascentia deam: est si! Abstulit iacent duxisses curarum est pronus dixit!
